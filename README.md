# LOW POLY ENVIRONMENT GENERATOR

## This package contains the following
- A tool to create low poly land, regioning with different colours, varying heights and level of detail.
- A prefab distribution tool that will allow you to scatter and clump objects on your mesh. Create fantastical environments layer by layer.
- Example assets for demostrational purposes.


# HOW TO USE

## Base mesh generation
--STEP 1
- Create a new scene.
- Create a **manager** game object (An empty that will host the generation scripts).
- Add the **Map Generator** and **Map Display** scripts to the manager.

--STEP 2
- Add a new empty game object to the scene.  Now known as **BASEMESH.**
- Add a **Mesh Renderer** and a **Mesh Filter** component to the game object.
- Add the **mat_Mesh** material to the object, from the **Material** folder.
- Add the **mat_Mesh** material to the materials tab in the **Mesh Renderer.**
- Add the **Foliage Generator** script to the manager

--STEP 3
- Add a new plane to the scene (Map Display).
- On the manager under the **Map Display** script, assign the **Map Display GameObject** to the Tex Rend variable.
- Assign **BASEMESH** to the **Mesh Filter** and **Mesh Renderer** components on the manager.
- Change the **Octave** value from 0 to 1
- Set the animation curve in **Mesh Height Curve**
- Set the mesh height
- Set **Draw Mode** to Mesh
- Begin fiddling with some settings.
## Foliage Generation
--STEP 1
- Make sure that **Foliage Generator** is attached to the manager
- Add a new Foliage Data.
- Add a new child to the **Base Mesh**.
- Assign the child to the **Foliage Parent** serialized property in the **Foliage Generator** script.
- Tick on**Spawn Object** and **Destroy On Generate**.
- Play around an experiment.

# ADVICE
- Make sure desired objects to be scattered has LOD's as this is mesh generated.
- Objects are scaled correctly, height can be adjusted in the tool under **Foliage Size**.
- Change noise scale to avoid pixels.
- Untick the 2 bools once you are done making a foliage layer in the **Foliage Generator** script to avoid re shuffling.
-  Add the attached **Foliage Height Correction** to the desired prefab and give it a distance to attach the prefab origin to the mesh.
# NICE TO READ
- This tool can be used for more than forest generation, using the provided serialised fields one can create some very interesting effects.


# CREDITS
- Sebastian Lague - Procedural Landmass Generation Episode 1-3
[Click Here To See](https://www.youtube.com/watch?v=wbpMiKiSKm8&list=PLB7vzdRuAzMhoDobILxNTe1MhNEPzIkjk&index=2&t=0s&ab_channel=SebastianLague)
- I followed the first 2 episodes to learn the theory about land, to refresh my understanding about octaves, amplitueds and frequency.
- I used the third videos Noise class as reference to create my own.
- From episode 4 - 6, I noted Sebastians logic to then create my own system with a new understanding on how to approach it.